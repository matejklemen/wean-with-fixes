import os
import argparse
from pandas import read_csv
import sys
sys.path.append('../')
from utils import jsonl

parser = argparse.ArgumentParser(description="process_pnli.py")
parser.add_argument("--data_dir", help="Path to directory containing the dataset (train.csv, dev.csv, test.csv)",
                    required=True)
parser.add_argument("--dst_dir", help="Path to directory where the preprocessed dataset should be placed",
                    required=True)
opt = parser.parse_args()


def transform(dataset_path: str) -> list:
    # Assume csv structure with two columns: 'sent1' and 'sent2' which get renamed to 'source' and 'target'
    print(f"**Processing {dataset_path}**")
    processed_data = []
    df = read_csv(dataset_path, header=0)
    for i in range(df.shape[0]):
        processed_data.append({
            "source": df.iloc[i]["sent1"].strip().lower(),
            "target": df.iloc[i]["sent2"].strip().lower()
        })

    return processed_data


if __name__ == '__main__':
    train_set = transform(os.path.join(opt.data_dir, "train.csv"))
    dev_set = transform(os.path.join(opt.data_dir, "dev.csv"))
    test_set = transform(os.path.join(opt.data_dir, "test.csv"))

    print(f"**Writing processed data to '{opt.dst_dir}': train.jsonl, dev.jsonl, test.jsonl**")
    with open(os.path.join(opt.dst_dir, "train.jsonl"), "w") as f_train:
        jsonl.dumps(train_set, f_train)
    with open(os.path.join(opt.dst_dir, "dev.jsonl"), "w") as f_dev:
        jsonl.dumps(dev_set, f_dev)
    with open(os.path.join(opt.dst_dir, "test.jsonl"), "w") as f_test:
        jsonl.dumps(test_set, f_test)
